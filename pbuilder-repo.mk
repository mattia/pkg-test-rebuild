.PHONY: all

export umask 002

#all: Release InRelease Release.gpg
all: Release

BINARY_PACKAGES = $(sort $(wildcard *.deb))
SOURCE_PACKAGES = $(sort $(wildcard *.dsc))
#GPG_KEY := 49B6574736D0B637CC3701EA5DB7CA67EA59A31F
#SECRING := /home/groups/reproducible/private/reproducible-private.gpg
#PUBRING := /home/groups/reproducible/private/pubring.gpg
#GPG_OPTS := --no-default-keyring --secret-keyring=$(SECRING) --keyring=$(PUBRING) -u $(GPG_KEY) --digest-algo SHA512

MV := mv -f

Release: Packages Sources Packages.gz Sources.gz
	apt-ftparchive -o APT::FTPArchive::Release::Origin=localrepo \
		release . > $@.new
	$(MV) $@.new $@
	chmod 664 $@

InRelease: Release
	gpg $(GPG_OPTS) --clearsign -o - Release > $@.new
	$(MV) $@.new $@
	chmod 664 $@

Release.gpg: Release
	gpg $(GPG_OPTS) -o - -abs Release > $@.new
	$(MV) $@.new $@
	chmod 664 $@

Packages: $(BINARY_PACKAGES)
	apt-ftparchive packages . > $@.new
	$(MV) $@.new $@
	chmod 664 $@

Sources: $(SOURCE_PACKAGES)
	apt-ftparchive sources . > $@.new
	$(MV) $@.new $@
	chmod 664 $@

Packages.gz: Packages
	gzip -n9 < $< > $@.new
	$(MV) $@.new $@
	chmod 664 $@

Sources.gz: Sources
	gzip -n9 < $< > $@.new
	$(MV) $@.new $@
	chmod 664 $@

