#!/bin/bash

set -eu

typ=(pkgs first second result)
for t in "${typ[@]}"; do
    if [ ! -d "$t" ]; then
        echo "E: Expected directory $t is missing in CWD" >&2
        exit 1
    fi
done


shopt -s nullglob  # don't bother if this glob here doesn't match anything
for r in first second result; do
    for t in "$r"/*.FAIL "$r"/*.CHECK; do
        if grep -q -E '^https://bugs.debian.org' "$t"; then
            bug=$(sed -e 's,^https://bugs\.debian\.org/\([0-9]\{6\,7\}\).*$,\1,' "$t")
            printf "Checking %s: " "$bug"
            done=$(bts status "$bug" fields:done|awk '{print $2}')
            if [ -n "$done" ]; then
                echo "✔ closed: $t"
            else
                echo ✘
            fi
        fi
    done
done
