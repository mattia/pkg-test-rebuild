#!/bin/bash

# This script draft the bug body, subject and attachment and puts it in
# a directory that later mutt_send_bugs.sh can pick up.

set -eu

if [ "${1:-}" = "--nolog" ]; then
    shift
    nolog=y
fi

dir=${1?pass the directory as first argument}
pkg=${2?pass a .dsc name as second argument}

arch=$(dpkg --print-architecture)

if [ -d "../$dir" ]; then
    # we are already in $dir
    cd ..
fi

p=$(basename "$pkg" .dsc|cut -d_ -f1)
v=$(basename "$pkg" .dsc|cut -d_ -f2)

log="$dir/${p}_${v}_${arch}.build"
if [ "${nolog:-}" != y ]; then
    less "$log"
fi
read -r -e -p "file bug? [Y/n] " -i y y
if [ "$y" != "y" ]; then
   echo "Then, exiting."
   exit
fi

d=~/mails/"${p}_$v"
mkdir -p "$d"

echo "$p: FTBFS: " > "$d/subject"
cp "$log" "$d/log"

cat > "$d/body" <<EOF
Source: $p
Version: $v
Severity: serious
Tags: ftbfs

Dear maintainer,

your package failed to rebuild in a standard sid/${arch} chroot.
Attached is the full build log, hopefully relevant excerpt follows:


EOF

