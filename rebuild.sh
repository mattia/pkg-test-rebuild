#!/bin/bash

set -eu

check_shutdown() {
    if [ -e ~/STOP ]; then
        echo "W: stop file exists, exiting" >&2
        exit 0
    fi
}

check_shutdown

if [ ! -f list ]; then
    echo "E: list file is missing" >&2
    exit 1
fi

# unset all potentially bad LC_* and LANG variables
while read -r var; do unset "$var"; done < <(env | grep  '^LC_' | cut -d= -f1)
unset LANG

list=()
while read -r p; do
    list+=("$p")
done < list

if [ ${#list} -eq 0 ]; then
    echo "E: empty list" >&2
    exit 1
fi

mkdir -p pkgs first second result

function check_done() {
    # $1 → dir
    # $2 → pkg
    local i
    for i in SKIP OK FAIL CHECK; do
        if [ -e "$1/$pkg.$i" ]; then
            return 1
        fi
    done
}

echo "@@@@@@@@@@@@@@@ Downloading"

# first download everything
pushd pkgs
for pkg in "${list[@]}"; do
    check_shutdown
    if [ "${pkg:0:1}" = "#" ]; then
        echo "I: skipping $pkg"
        continue
    fi
    if compgen -G "${pkg}_*.dsc" > /dev/null; then
        echo "I: $pkg alredy download"
        continue
    fi
    apt-get source -d --only-source "$pkg"/unstable
done
popd


echo "@@@@@@@@@@@@@@@ First build"

# then build everything in a base sid chroot
for pkg in pkgs/*dsc; do
    check_shutdown
    pkg=$(basename "$pkg")
    if ! check_done first "$pkg"; then
        echo "I: $pkg already tried, skipping"
        continue
    fi
    set -x
    if sudo -E pbuilder build --buildresult first \
        --basetgz first.tgz \
        --pkgname-logfile pkgs/"$pkg";
    then
        touch first/"$pkg".OK
    else
        touch first/"$pkg".FAIL
    fi
    set +x
done


echo "@@@@@@@@@@@@@@@ Second build"

# then rebuild everything using a special chroot
for pkg in pkgs/*dsc; do
    check_shutdown
    pkg=$(basename "$pkg")
    if [ -e first/"$pkg".FAIL ]; then
        echo "I: $pkg failed the first build, skipping the second" | tee second/"$pkg".SKIP
        continue
    fi
    if [ -e first/"$pkg".CHECK ] || [ -e first/"$pkg".SKIP ]; then
        echo "I: $pkg was skipped in the first build, skipping the second too" | tee second/"$pkg".SKIP
    fi
    if ! check_done second "$pkg"; then
        echo "I: $pkg already tried, skipping"
        continue
    fi
    set -x
    if sudo -E pbuilder build --buildresult second \
            --basetgz second.tgz \
            --pkgname-logfile pkgs/"$pkg";
    then
        touch second/"$pkg".OK
    else
        touch second/"$pkg".FAIL
    fi
    set +x
done


echo "@@@@@@@@@@@@@@@ Comparing"

# lastly, compare the two builds
for pkg in pkgs/*.dsc; do
    check_shutdown
    pkg=$(basename "$pkg")
    if ! check_done result "$pkg"; then
        echo "I: $pkg already checked, skipping"
        continue
    fi
    if [ ! -e first/"$pkg".OK ] || [ ! -e second/"$pkg".OK ]; then
        echo "I: $pkg didn't build fine in either the first or second build, not comparing"
        for typ in CHECK FAIL SKIP; do
            for round in first second; do
                if [ -e "$round/$pkg.$typ" ]; then
                    echo -n "$round/$pkg.$typ exists: " > result/"$pkg".SKIP
                    cat "$round/$pkg.$typ" >> result/"$pkg".SKIP
                    continue 3
                fi
            done
        done
        echo "CAN'T UNDERSTAND WHY .OK IS MISSING" > result/"$pkg".SKIP
        continue
    fi
    p=$(basename "$pkg" .dsc)
    changes="${p}_amd64.changes"
    if ! debdiff "first/$changes" "second/$changes" > "result/${p}.diff" 2>&1 ; then
        echo "I: $pkg has differences"
        touch result/"$pkg".CHECK
    else
        echo "I: $pkg is the same"
        touch result/"$pkg".OK
    fi
done
