#!/bin/bash

set -eu

typ=(pkgs first second result)
for t in "${typ[@]}"; do
    if [ ! -d "$t" ]; then
        echo "E: Expected directory $t is missing in CWD" >&2
        exit 1
    fi
done

mkdir -p completed/{pkgs,first,second,result}

shopt -s nullglob  # don't bother if this glob here doesn't match anything
for i in result/*.dsc.OK ; do
    pkg=$(basename "$i" .dsc.OK)
    echo "I: Moving $pkg..."
    for t in "${typ[@]}"; do
        mv -n -v "$t/$pkg"* completed/"$t"/
    done
done

echo "I: Done."
