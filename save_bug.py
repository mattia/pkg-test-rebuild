#!/usr/bin/python3

import re
import fileinput

from pathlib import Path


def parse_line(line: str):
    # [06:39:11 PM] <BTS> Opened #948901 (serious) in src:foo 1-1 by M R (mattia) «o». https://bugs.debian.org/948901
    # [06:39:11 PM] <BTS> Opened #948901 in src:foo 1-1 by M R (mattia) «o». https://bugs.debian.org/948901
    # [06:39:11 PM] <BTS> Opened #948901 in src:foo 1-1 by M R <m@d..> «o». https://bugs.debian.org/948901
    r = re.compile(
        r'^'
        r'.*\s<BTS>\sOpened\s'                              # lead
        r'#(?P<bnr>\d{6,7})\s'                              # bug number
        r'(?:\((?P<severity>serious|grave|critical)\)\s)?'  # severity
        r'in\s(?:src:)?(?P<pkg>[a-z0-9][a-z0-9.+-]+)\s'     # package name
        r'(?P<version>[0-9][A-Za-z0-9:.+~-]+)?\s?'          # pkg version
        r'by.*[)>]\s'                                       # bug author
        r'«(?P<title>.+)»\.\s'                              # bug title
        r'(?P<url>https://bugs\.debian\.org/\d{6,7})'       # bug url
        r'$',
        re.MULTILINE,
    )
    result = r.match(line)
    return result


def yes_or_no(question):
    while True:
        reply = input(question+' [y/n]: ').lower().strip()
        if not reply:
            continue
        if reply[0] == 'y':
            return True
        if reply[0] == 'n':
            return False


def save(parsed_line):
    line = f"{parsed_line['url']} «{parsed_line['title']}»"
    pwd = Path('.')
    for pth in ('first', 'second', 'result'):
        for typ in ('CHECK', 'FAIL', 'SKIP'):
            f = f"{parsed_line['pkg']}_{parsed_line['version']}.dsc.{typ}"
            p = pwd / pth / f
            if p.exists() and p.stat().st_size == 0:
                if yes_or_no(f'Save in «{p}»?'):
                    p.write_text(line)
                else:
                    print('Not saving, please write this yourself:')
                    print(line)
                    input('Saved?')
                print()
                print('Next bug:')
                return
    print("ERROR: Can't figure out where to save the line")
    print('Please write this yourself:')
    print(line)
    input('Saved?')
    print()
    print('Next bug:')


def main():
    print('One bug per line, terminated by either ^D or empty line:')
    print()
    for line in fileinput.input():
        if not line.strip():
            break
        r = parse_line(line)
        parsed = r.groupdict()
        print()
        print('Parsed bug:')
        if not r['version']:
            parsed['version'] = 'NoneVersion'
        parsed['severity'] = f" ({r['severity']})" if r['severity'] else ''
        print(f"#{r['bnr']} in "
              f"{r['pkg']}/{parsed['version']}{parsed['severity']}")
        print(f"\t{parsed['title']}")
        if yes_or_no("Do you want to save?"):
            save(parsed)
        else:
            print("Next bug:")


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
    print('Goodbye')
