#!/bin/bash

# Run this in a directoy containing stuff as done by draft_bug.sh
# Usually you'd want to rsync said directory from the builder into
# your workstation where you have a running `mutt`.

set -eu

send_mail() {
    # $1 → subdirectory
    pushd "$1" > /dev/null
    mutt \
        -s "$(<subject)" \
        -i body \
        -a log \
        -- \
        submit@bugs.debian.org
        echo "$(date) - Mail sent for $1" | tee "done"
    popd > /dev/null
}

for i in *; do
    if [ ! -d "$i" ]; then
        # skip non-dirs
        continue
    fi
    if [ -f "$i/done" ] || [ -f "$i/skip" ]; then
        continue
    fi
    for expected_file in subject body log ; do
        if [ ! -f "$i/$expected_file" ]; then
            echo "Expected file is missing $i/$expected_file" >&2
            exit 1
        fi
    done
    p=$(echo "$i"|cut -d_ -f1)
    echo "Dealing with $i"
    # https://github.com/koalaman/shellcheck/issues/1796
    # shellcheck disable=SC2034
    while true; do
        read -r -e -p "Do you want to see the log? [l/y/n] " -i l y
        case "$y" in
            n) ;;
            y) sensible-browser "https://tracker.debian.org/$p"
               sensible-browser "https://tests.reproducible-builds.org/debian/$p"
               sensible-browser "https://bugs.debian.org/src:$p"
               sensible-browser "file://$PWD/$i/log"
               ;&
            l) less "$i/log" ;;
        esac
        read -r -e -p "Again? [y/n] " -i n y
        if [ "$y" = "n" ]; then
            break
        fi
    done
    read -r -e -p "file bug? [Y/n] " -i y y
    if [ "$y" = y ]; then
        send_mail "$i"
    else
        echo "$(date) - Not filing bug for $i" | tee "$i/skip"
    fi
done
