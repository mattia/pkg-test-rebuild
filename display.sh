#!/bin/bash

set -eu
shopt -s nullglob  # don't bother if 'for i in *foo' doesn't mach anything

dir=${1:-first}

function count_files() {
    find "$dir" -name "$1" ! -name "*.swp" | wc -l
}

total_sources=$(find pkgs -name '*.dsc'|wc -l)
built=$(count_files '*.dsc.*')
oked=$(count_files '*.dsc.OK')
skipped=$(count_files '*.dsc.SKIP')
failed=$(count_files '*.dsc.FAIL')
needscheck=$(count_files '*.dsc.CHECK')

total_chk=$(( oked + skipped + failed + needscheck ))
if [ "$total_chk" -ne "$built" ]; then
    echo "MathError" >&2
    echo "$oked + $skipped + $failed == $total_chk != $built" >&2
    exit 1
fi

printf 'Building: %s\n' "$dir"
printf '%s/%s (failures: %s skipped: %s needscheck: %s)\n\n' "$built" "$total_sources" "$failed" "$skipped" "$needscheck"


cols=$(tput cols)
# 51 is how many columns the .dsc list takes from the printf below
cols=$((cols - 52))
function display_line() {
    local i typ
    i=$(basename "$1")
    # $1 → the .OK or .FAIL filename
    case "${i: -2}" in
        IL) typ=FAIL ;;
        IP) typ=SKIP ;;
        CK) typ=CHECK ;;
        OK) typ=OK ;;
        *) echo "unknwon type passed" >&2 ; exit 1 ;;
    esac
    if [ ${#i} -gt 45 ]; then
        # damn long package names/versions
        # (also mind the `local`, otherwise the overriden
        # variable here would leak to the next cycle)
        local cols=$(( cols + 45 - ${#i} ))
    fi
    printf '%-5s: %-45s %.*s\n' "$typ" "$(basename "$i" ."$typ")" "$cols" "$(head -n1 "$dir/$i")"
}

lines=$(tput lines)
# 5 is the lines I need for other things, headers, etc.
lines=$(( lines -5 ))
if [ "$built" -gt "$lines" ]; then
    printf 'Too many built packages, diplaying the failures first.\n'
    for typ in CHECK FAIL SKIP; do
        for i in "$dir"/*.dsc."$typ"; do
            display_line "$i"
        done
    done
    # shellcheck disable=SC2012
    for i in $(ls -1t "$dir"/*.dsc.OK|head -n $(( lines - failed - skipped ))); do
        display_line "$i"
    done
else
    for i in "$dir"/*.dsc.*; do
        display_line "$i"
    done
fi
