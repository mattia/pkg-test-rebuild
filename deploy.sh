#!/bin/sh

set -eux

if ! grep -q -E 'deb-src.*unstable.*main' /etc/apt/sources.list; then
    echo "deb-src http://mirrors.digitalocean.com/debian unstable main" | sudo tee -a /etc/apt/sources.list > /dev/null
fi
sudo apt update

sudo apt install --no-install-recommends screen pbuilder devscripts eatmydata htop iotop tree zsh python3-minimal colordiff libsoap-lite-perl

for dotfile in screenrc pbuilderrc ; do
    ln -sf "$PWD/$dotfile" ~/."$dotfile"
done

mkdir -p ~/pbuilder/hooks ~/pbuilder/repository
ln -sf "$PWD/E10speedup" ~/pbuilder/hooks/
ln -sf "$PWD/pbuilder-repo.mk" ~/pbuilder/repository/Makefile
make -C ~/pbuilder/repository -B

mkdir -p ~/workspace ~/bin

for script in draft_bug.sh display.sh rebuild.sh save_bug.py filter_out_complted.sh check_bug_status.sh check_ftbfs.sh; do
    ln -sf "$PWD/$script" ~/bin/
done

cd ~/workspace
sudo -E pbuilder create --basetgz first.tgz
sudo -E pbuilder update --basetgz first.tgz --override-config
cp first.tgz second.tgz
printf '\n\n\n\n\t\t\tREMEMBER TO MODIFY second.tgz APPROPRIATELY\n\n\n\n'
