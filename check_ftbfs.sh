#!/bin/bash

set -eu

dir=${1=pass a directory as first parameter}
dsc=${2?pass a .dsc as second parameter}
arch=$(dpkg --print-architecture)

fail="$dir/$dsc.FAIL"

if [ ! -f "$fail" ]; then
    echo "ENOENT: $fail" >&2
    exit 1
fi

if [ -s "$fail" ]; then
    # https://github.com/koalaman/shellcheck/issues/1796
    # shellcheck disable=SC2034
    read -r -e -p "Failure for $dsc already investigated, check again? " -i n y
    if [ "$y" = "n" ]; then
        echo "Exiting, then."
        exit 0
    fi
fi

log="$dir/$(basename "$dsc" .dsc)_${arch}.build"
less "$log"

read -r -e -p "What next? [file new Bug/Save bug/Retry/Nothing] " y
case "$y" in
    b) draft_bug.sh --nolog "$dir" "$dsc" ;;
    s) vim "$fail" ;;
    r) rm -v "$fail" "$log" ;;
    n) exit 0 ;;
    *) echo "unknown option: $y" >&2 ; exit 1;;
esac
